package com.example.contactosllamadas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lvContactos;
    ArrayList<Contacto> contactos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvContactos = (ListView) findViewById(R.id.lvContactos);
        contactos = new ArrayList<>();
        contactos.add(new Contacto("Luis Seen","Gomez Monrroy",553212093,"Zumpango"));
        contactos.add(new Contacto("Lucero","Chavez Gomez",550100212,"Ojo Agua"));
        contactos.add(new Contacto("Carlos Alexander","Franco G",55212,"Zumpango"));
        contactos.add(new Contacto("Isai","Almaraz Becerro",55,"San Juan"));
        contactos.add(new Contacto("Veronica G","Diaz",556544955,"Zumpango"));

        ArrayList<String> nombreContactos = new ArrayList<>();

        for(Contacto contacto: contactos){ // -- Recorre el arreglo para tomar los nombres y mostrarlos en la lista
            nombreContactos.add(contacto.getNombre()+" "+contacto.getApellido());
        }

        lvContactos.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nombreContactos));
        lvContactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int posicion, long id) {
                Intent intent = new Intent( MainActivity.this, DetalleContacto.class);
                intent.putExtra("NOMBRECONTACTO",contactos.get(posicion).getNombre());
                intent.putExtra("APELLIDOCONTACTO",contactos.get(posicion).getApellido());
                intent.putExtra("TELEFONOCONTACTO",contactos.get(posicion).getTelefono());
                intent.putExtra("DIRECCIONCONTACTO",contactos.get(posicion).getDireccion());
                startActivity(intent);
            }
        });
    }
}