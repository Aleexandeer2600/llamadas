package com.example.contactosllamadas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetalleContacto extends AppCompatActivity {

    TextView tvNombre, tvApellido, tvTelefono, tvDireccion;
    Button buttonNumero;
    int telefono;
    private final int PHONE_CALL_CODE = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_contacto);

        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("NOMBRECONTACTO");
        String apellido = bundle.getString("APELLIDOCONTACTO");
        telefono = bundle.getInt("TELEFONOCONTACTO");
        String direccion = bundle.getString("DIRECCIONCONTACTO");

        tvNombre = (TextView) findViewById(R.id.tvNombre);
        tvApellido = (TextView) findViewById (R.id.tvApellido);
        tvTelefono = (TextView) findViewById(R.id.tvTelefono);
        tvDireccion = (TextView) findViewById(R.id.tvDireccion);
        buttonNumero = (Button) findViewById (R.id.btnCall);

        tvNombre.setText(nombre);
        tvApellido.setText(apellido);
        tvTelefono.setText("" + telefono);
        tvDireccion.setText(direccion);


        buttonNumero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(telefono != 0){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){  //-Version donde esta la app >= version Marshmallow Android6.0
                    //  versionNueva();   // - pide permisos de la forma nueva
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                }
            }

            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                // -- condicion para comprobar si se la a concedido el permiso
                if(result == PackageManager.PERMISSION_GRANTED){
                    // -- Concedio permiso
                    Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+telefono));
                    if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) return;
                    startActivity(llamada);
                }else{
                    // - No concedio permiso
                    Toast.makeText(this, "No acepto los permisos", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }


}